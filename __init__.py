#This file is part product_barcode module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from . import product
# from . import position

def register():
    Pool.register(
        # position.ProductPosition,
        product.Product,
        product.Template,
        module='product_onebarcode', type_='model')
